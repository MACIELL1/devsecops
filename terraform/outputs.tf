output "ecr_name" {
  value = "${aws_ecr_repository.repository.name}"
}

output "repository_url" {
  value = "${aws_ecr_repository.repository.repository_url}"
}

output "cluster_name" {
  value = "${aws_ecs_cluster.cluster_name.name}"
}

