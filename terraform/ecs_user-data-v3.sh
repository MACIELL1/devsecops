#!/bin/bash
echo ECS_CLUSTER=${cluster_name} >> /etc/ecs/ecs.config
echo 'ECS_AVAILABLE_LOGGING_DRIVERS=["json-file","syslog","fluentd","awslogs"]' >> /etc/ecs/ecs.config
sudo docker plugin install --grant-all-permissions rexray/ebs EBS_REGION=us-east-1 REXRAY_PREEMPT=true LINUX_VOLUME_FILEMODE=0755
sudo stop ecs && sleep 3 && sudo start ecs

sleep 5

# sudo chown ec2-user.ec2-user /opt/ -R
# sudo su ec2-user -c "git clone git@bitbucket.org:team/bootstrap.git /opt/bootstrap"

sleep 50
echo"DONE"

# # Primeiro playbook - Alteracao de hostname
# sudo /usr/local/bin/ansible-playbook /opt/bootstrap/setup.yml -e 'ansible_python_interpreter=/usr/bin/python'

# # Instalacao node_exporter
# sudo /usr/local/bin/ansible-playbook /opt/bootstrap/custom-role.yml -e 'ansible_python_interpreter=/usr/bin/python roles=node_exporter'