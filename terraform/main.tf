terraform {
  backend "s3" {
    encrypt = true  
    bucket = "tf-state"
    key    = "ecscluster-prd/terraform.state"
    region = "us-east-1"
    profile = "default"
  }
  resource "aws_ecr_repository" "repository" {
  name = "${var.ecr_name}"
}
resource "aws_ecr_lifecycle_policy" "policy" {
  repository = "${var.repository}"

  policy = <<EOF
{
    "rules": [
        {
            "rulePriority": 10,
            "description": "Keep the last 5 images",
            "selection": {
                "tagStatus": "any",
                "countType": "imageCountMoreThan",
                "countNumber": 5
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
EOF
}
resource "aws_ecs_cluster" "cluster_name" {
  name = "${var.cluster_name}"
}

}